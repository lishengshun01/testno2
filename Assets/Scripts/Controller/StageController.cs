﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageInfo
{
    public float[] obstaclePosX;
    public float[] obstaclePosY;
    public float[] obstaclePosZ;
}

public class StageController : SingletonMonoBehavior<StageController>
{
    public GameObject obstacle;
    public GameObject[] obstacles = new GameObject[4];
    //public SoapCubeController obstacle;
    //public StageInfo stageInfo;
    //public GameObject[] currentObstacles = new GameObject[4];
    //List<GameObject[]> listObstacles = new List<GameObject[]>();

    //int currentObstacleNumber = 0;
    // Start is called before the first frame update
    void Start()
    {
        //stageInfo = DataController.stageVO.GetArrStage();
        spawnObstacles();
    }

    public void spawnObstacles()
    {
        int currentRow = 0;

        for(int i = 0; i < 4; i++)
        {
            Vector3 startPos = new Vector3(0, 5.81f + (4-i), 0);
            GameObject oj = Instantiate(obstacle, startPos, Quaternion.Euler(0, 0, 0)).gameObject;
            obstacles[i] = oj;
            if (i > 1)
            {
                //oj.SetActive(false);

                var a = obstacles[i].GetComponentsInChildren<BoxCollider>();
                for (int j = 0; j < a.Length; j++)
                {
                    //a[j].isTrigger = true;
                    a[j].enabled = false;
                }
            }
        }

        //for (int z = 0; z < 6; z++)
        //{
        //    for (int i = 0; i < 1; i++)
        //    {
        //        int currentColumn = 0;
        //        for (int j = 0; j < 12; j++)
        //        {
        //            Vector3 startPos = new Vector3(0 + z, 5.81f + i, 0 + j);
        //            GameObject oj = Instantiate(obstacle, startPos, Quaternion.Euler(0, 0, 0)).gameObject;
        //            if (i > 1)
        //            {
        //                oj.SetActive(false);
        //            }
        //            //SoapCubeController cube = Instantiate(obstacle, startPos, Quaternion.Euler(0, 0, 0)) as SoapCubeController;
        //            //cube.row = 8-i;
        //            currentColumn++;
        //        }
        //        currentRow++;
        //    }
        //}
    }

    public void setOjActive()
    {

    }

    //public void onReplay()
    //{
    //    currentObstacleNumber = 0;
    //    for (int i = 0; i < currentObstacles.Length; i++)
    //    {
    //        Destroy(currentObstacles[i]);
    //    }
    //    spawnObstacles();
    //}
}
