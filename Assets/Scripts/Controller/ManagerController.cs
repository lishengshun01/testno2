﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TOPICNAME
{
    public const string WIN = "Win";
    public const string LOSE = "Lose";
    public const string Perfect = "Perfect";
}
public enum GameState
{
    IsPlaying,
    IsOver,
    None
}

public class ManagerController : Singleton<ManagerController>
{
    public static int Life = 3;
    public PlayerController player;
    public UIController ui;

    void Start()
    {

    }

    public void OnWin()
    {
    }

    public void OnLose()
    {
    }

    public void onReplay()
    {
    }
}
