﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoapCubeController : MonoBehaviour
{
    public Material[] materials;
    public int row;
    public Transform blade;
    Rigidbody rigidbody;
    public bool done = false;
    PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        player = ManagerController.Instance.player;
        int rng = Random.Range(0, 2);
        GetComponent<MeshRenderer>().material = materials[rng];
        rigidbody = GetComponent<Rigidbody>();
        //rigidbody.Sleep();
    }
}
