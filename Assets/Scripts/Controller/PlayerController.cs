﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//needs more fix on player movement

public class PlayerController : BehaviorController
{
    public static int currentRow = 1;
    int counter = 0;
    Vector3 startPos = Vector3.zero;
    bool movingLeft = true;
    GameObject[] obstacles;
    private void Start()
    {
        obstacles = StageController.Instance.obstacles;
        ManagerController.Instance.player = this;
        counter = 0;
        currentRow = 1;
        movingLeft = true;
        startPos = transform.position;
        Physics.gravity = new Vector3(0, -44, 0);
    }

    public void onUpdatePlayer()
    {
    }

    private void Update()
    {
        if (currentRow > 4) enabled = false;
        getTouch();
        //getMouse();
    }

    void getMouse()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //currentRow++;
        }
        if (Input.GetKey(KeyCode.Mouse0))
        {
            transform.position = getMouseWorldPos();
            //transform.rotation = Quaternion.Euler(0, 153, -2);
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            if (counter >= 72)
            {
                counter = 0;
                currentRow++;
                startPos = new Vector3(startPos.x, startPos.y - (1 * 1), startPos.z);
                if (currentRow > 3) return;
                var a = obstacles[currentRow].GetComponentsInChildren<BoxCollider>();
                for (int i = 0; i < a.Length; i++)
                {
                    //a[i].isTrigger = false;
                    a[i].enabled = true;
                }
                Debug.Log(currentRow);
            }
            if (currentRow <= 4)
                transform.position = startPos;


            //transform.position = new Vector3(startPos.x, startPos.y-(1*0.8f), startPos.z);
            //startPos = transform.position;
        }
    }

    void getTouch()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Ended) //mouse up
            {
                if (counter >= 71)
                {
                    counter = 0;
                    currentRow++;
                    startPos = new Vector3(startPos.x, startPos.y - (1 * 1), startPos.z);
                    if (currentRow > 3) return;
                    var a = obstacles[currentRow].GetComponentsInChildren<BoxCollider>();
                    for (int i = 0; i < a.Length; i++)
                    {
                        //a[i].isTrigger = false;
                        a[i].enabled = true;
                    }
                    Debug.Log(currentRow);
                }
                if (currentRow <= 4)
                    transform.position = startPos;
            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                transform.position = getTouchWorldPos();
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        SoapCubeController soapPiece = collision.gameObject.GetComponent<SoapCubeController>();
        if (soapPiece.done)
        {
            //collision.transform.position += new Vector3(0.5f, 0, 0.5f);
            return;
        }
        //Debug.Log(soapPiece);
        Rigidbody rigidbody = collision.gameObject.GetComponent<Rigidbody>();
        //rigidbody.detectCollisions = false;
        if (rigidbody)
        {
            //Debug.Log("A");
            if(!soapPiece.done)
            {
                soapPiece.done = true;
                counter++;
            }
            //Debug.Log(counter);
            //Physics.gravity = new Vector3(0, -22, 0);
            collision.transform.position += new Vector3(Random.Range(0f, 0.8f), 1f, Random.Range(0f, 0.8f));
            rigidbody.constraints = RigidbodyConstraints.None;

            rigidbody.velocity = new Vector3(2, 25, 2);
            //rigidbody.detectCollisions = false;
            //rigidbody.useGravity = true;
        }
    }

    //private void OnCollisionStay(Collision collision)
    //{
    //    if (collision.gameObject.GetComponent<SoapCubeController>().done)
    //    {
    //        //collision.transform.position += new Vector3(0.2f, 0, 0.2f);
    //        collision.transform.position += collision.transform.forward * 0.2f;
    //    }
        
    //}

    Vector3 getMouseWorldPos()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        mousePoint = Camera.main.ScreenToWorldPoint(mousePoint);
        mousePoint.y = transform.position.y;
        
        return mousePoint;
    }

    Vector3 getTouchWorldPos()
    {
        Vector3 touchPoint = Input.GetTouch(0).position;
        touchPoint.z = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        touchPoint = Camera.main.ScreenToWorldPoint(touchPoint);
        touchPoint.y = transform.position.y;
        return touchPoint;
    }
}