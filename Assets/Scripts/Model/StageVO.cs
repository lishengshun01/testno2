﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;


public class StageVO : BaseVO
{
    public void LoadData()
    {

        ReadData(DataConfig.PathStage);
        data = data[KeyConfig.data].AsArray;
    }

    public float[] getArrList(string keyConfig)
    {
        JSONNode ObstaclesPos = data[0][keyConfig].AsArray;
        float[] obstaclesPos = new float[ObstaclesPos.Count];
        for (int j = 0; j < ObstaclesPos.Count; j++)
        {
            obstaclesPos[j] = ObstaclesPos[j].AsFloat;
        }
        return obstaclesPos;
    }

    public StageInfo GetArrStage()
    {
        StageInfo[] stageInfo = new StageInfo[data.Count];
        for (int i = 0; i < stageInfo.Length; i++)
        {
            stageInfo[i] = new StageInfo();
            //JSONNode ObstaclesPosX = data[i][KeyConfig.obstaclePosX].AsArray;
            //float[] obstaclesPosX = new float[ObstaclesPosX.Count];
            //for(int j = 0; j < ObstaclesPosX.Count; j++)
            //{
            //    obstaclesPosX[j] = ObstaclesPosX[j].AsFloat;
            //}
            //stageInfo[i].obstaclePosX = obstaclesPosX;
            stageInfo[i].obstaclePosX = getArrList(KeyConfig.obstaclePosX);
            stageInfo[i].obstaclePosY = getArrList(KeyConfig.obstaclePosY);
            stageInfo[i].obstaclePosZ = getArrList(KeyConfig.obstaclePosZ);

            //JSONNode ObstaclesPosY = data[i][KeyConfig.obstaclePosY].AsArray;
            //float[] obstaclesPosY = new float[ObstaclesPosY.Count];
            //for (int j = 0; j < ObstaclesPosY.Count; j++)
            //{
            //    obstaclesPosY[j] = ObstaclesPosY[j].AsFloat;
            //}
            //stageInfo[i].obstaclePosY = obstaclesPosY;
        }
        return stageInfo[0];
    }
}
